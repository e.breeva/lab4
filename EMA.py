import copy
def ema(spisok,n):
    s1=copy.deepcopy(spisok)
    k=2/(n+1)
    for i in range(len(s1)):
        if i==0:
            s1[i]=s1[i]
        else:
            s1[i]=s1[i]*k+s1[i-1]*(1-k)
    return(s1)
