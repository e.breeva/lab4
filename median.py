import copy
def med(spisok,n):    
    s1=copy.deepcopy(spisok)
    rez=[]
    for i in range(len(s1)-n):
        j=0
        window=[]
        while j<n:
            window.append(s1[i+j])
            j+=1
        window=sorted(window)
        m = int(len(window) / 2)
        middle = window[m]
        rez.append(middle)
    return(rez)
