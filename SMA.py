import copy
def sma(spisok,n):
    s1=copy.deepcopy(spisok)
    i=len(s1)-1
    while i>=n:
        j=0
        temp=0
        while j<n:
            temp+=s1[i-j]
            j+=1            
        s1[i]=temp/n
        i-=1
    return(s1)
