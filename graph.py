import matplotlib.pyplot as plt
def graphic(nach,sma,ema,med):#,med):
    grx=[]
    grx1=[]
    for i in range(len(nach)):
        grx.append(i)
    for i in range(len(med)):
        grx1.append(i)
    plt.xlabel('дни')
    plt.ylabel('цена')
    plt.grid()
    plt.plot(grx,nach,grx,sma,grx,ema,grx1,med)
    plt.legend(["до","после(sma)","после(ema)","после(med)"])
    plt.show()
